# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=phosh
pkgver=0.2.299
pkgrel=1
pkgdesc="Shell PoC for the Librem5"
# Blocked on mips and s390x by gnome-session, gnome-settings-daemon, squeekboard and libhandy
# Blocked on ppc64le by gnome-session
arch="all !s390x !ppc64le !mips !mips64"
url="https://source.puri.sm/Librem5/phosh"
license="GPL-3.0-only"
depends="wayland-protocols phoc gnome-session bash dbus-x11 gnome-settings-daemon
	squeekboard libpulse dbus:org.freedesktop.Secrets elogind gnome-control-center"
makedepends="gtk+3.0-dev meson ninja gnome-desktop-dev libhandy-dev gcr-dev upower-dev
	linux-pam-dev git cmake pulseaudio-dev networkmanager-dev polkit-elogind-dev
	libsecret-dev feedbackd-dev"
subpackages="$pkgname-lang"
source="phosh.desktop
	sm.puri.OSK0.desktop
	"
options="!check" # Needs a running Wayland compositor

build() {
	git clone "https://source.puri.sm/Librem5/phosh" "phosh-master"
        cd "phosh-master"
	git submodule init
        git config --local submodule.subprojects/gvc.url "https://gitlab.gnome.org/GNOME/libgnome-volume-control.git"
        git submodule update
	cd ..
	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--buildtype=plain \
		./phosh-master output
	ninja -C output
}

check() {
	ninja -C output test
}

package() {
        ls
	DESTDIR="$pkgdir/" ninja -C $srcdir/output install

	install -D -m644 "$srcdir"/phosh.desktop \
		"$pkgdir"/usr/share/wayland-sessions/phosh.desktop

	install -D -m644 "$srcdir"/sm.puri.OSK0.desktop \
		"$pkgdir"/usr/share/applications/sm.puri.OSK0.desktop

}
sha512sums="6644870edbbbc6b88d6e19f7771d81dba1a11066c2b34e4c22736db73a2dfd0d4909b4967503059c35385c5139a834a5c06a3c56b148ba1275d7f089c0c5f33c  phosh.desktop
f97019598323276cf97ae62f04b6245983198e04b228ddc605835ee46845d9b88c6890fb86e97e4bb6f1ad73361437d9ed18c91e81fe1284a88cdcb92d3fdc69  sm.puri.OSK0.desktop"
